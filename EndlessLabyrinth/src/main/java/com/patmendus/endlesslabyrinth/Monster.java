package com.patmendus.endlesslabyrinth;

public class monster extends labyrinthObject{
		
	public monster(String name, int attack, int defense, int health){
		this.name = name;
		this.attack = attack;
		this.defense = defense;
		this.health = health;
		
		this.type = "monster";
	}
	
}
