package com.patmendus.endlesslabyrinth;

public class adventurer{

	private int attack;
	private int defense;
	private int health;
	
	private item item1;
	private item item2;
	private item item3;
	
	private int baseStat;
	private int level;
	private int tempDefense;
	
	//CONTSTRUCTOR
	public adventurer(){
		this.baseStat = 3;
		this.level = 0;
		
		this.attack = this.baseStat;
		this.defense = this.baseStat;
		this.health = this.baseStat;
		
		this.tempDefense = this.defense;
		
		item emptyItem = new item("Empty",0,0,0);
		this.item1 = emptyItem;
		this.item2 = emptyItem;
		this.item3 = emptyItem;
	}
	
	//ACCESSORS
	public int getAttack(){
		return this.attack;
	}
	public int getDefense(){
		return this.defense;
	}
	public int getHealth(){
		return this.health;
	}	
	public item getItem1(){
		return this.item1;
	}
	public item getItem2(){
		return this.item2;
	}
	public item getItem3(){
		return this.item3;
	}
	public int getTempDefense(){
		return this.tempDefense;
	}
	
	//MUTATORS
	
	/* 
	 * No mutators necessary for attack, defense, or health.
	 * Attributes can only be altered by leveling or items. 
	 */
	
	public void setItem1(item item){
		this.item1 = item;
		calcAttributes();
	}
	public void setItem2(item item){
		this.item2 = item;
		calcAttributes();
	}
	public void setItem3(item item){
		this.item3 = item;
		calcAttributes();
	}	
	
	//METHODS
	public void calcAttributes(){
		this.attack = baseStat + this.item1.attack + this.item2.attack + this.item3.attack + this.level;
		this.defense = baseStat + this.item1.defense + this.item2.defense + this.item3.defense + this.level;
		this.health = baseStat + this.item1.health + this.item2.health + this.item3.health + this.level;
	}	
	public void levelUp(){
		this.attack ++;
		this.defense ++;
		this.health ++;
		this.level ++;
	}
}
