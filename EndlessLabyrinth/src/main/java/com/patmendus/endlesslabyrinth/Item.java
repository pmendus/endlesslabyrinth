package com.patmendus.endlesslabyrinth;

public class item extends labyrinthObject{
	
	public item(String name, int attack, int defense, int health){
		this.name = name;
		this.attack = attack;
		this.defense = defense;
		this.health = health;
		
		this.type = "item";
	}
}
