package com.patmendus.endlesslabyrinth;

public abstract class labyrinthObject {

	protected int attack;
	protected int defense;
	protected int health;
	
	protected String name;
	protected String type;
	
	//ACCESSORS
	public int getAttack(){
		return this.attack;
	}
	public int getDefense(){
		return this.defense;
	}
	public int getHealth(){
		return this.health;
	}
	public String getName(){
		return this.name;
	}
	public String getType(){
		return this.type;
	}
	
	//MUTATORS
	public void setAttack(int attack){
		this.attack = attack;
	}
	public void setDefense(int defense){
		this.defense = defense;
	}
	public void setHealth(int health){
		this.health = health;
	}
	public void setName(String name){
		this.name = name;
	}
	
	public void scaleWithLevel(int level){
		int levelMod = (level/10) + 1;
		this.attack *= levelMod;
		this.defense *= levelMod;
		this.health *= levelMod;
	}
}
