package com.patmendus.endlesslabyrinth;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
	adventurerTest.class,
	labyrinthObjectTest.class,
	itemTest.class,
	monsterTest.class
})

public class TestSuite {
	
}
