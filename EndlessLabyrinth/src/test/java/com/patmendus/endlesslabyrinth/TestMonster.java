package com.patmendus.endlesslabyrinth;

import static org.junit.Assert.*;

import org.junit.Test;

public class monsterTest {

	monster testMonster = new monster("testMonster",3,4,5);
	
	@Test
	public void testItem() {
		assertTrue(testMonster.getName().equals("testMonster"));
		assertTrue(testMonster.getAttack() == 3);
		assertTrue(testMonster.getDefense() == 4);
		assertTrue(testMonster.getHealth() == 5);
		assertTrue(testMonster.getType().equals("monster"));
	}

}
