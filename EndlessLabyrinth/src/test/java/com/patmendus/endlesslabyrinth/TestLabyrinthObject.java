package com.patmendus.endlesslabyrinth;

import static org.junit.Assert.*;

import org.junit.Test;

public class labyrinthObjectTest {

	item testItem = new item("testItem",3,4,5);
	
	@Test
	public void testGetAttack() {
		assertTrue(testItem.getAttack() == 3);
	}
	
	@Test
	public void testGetDefense() {
		assertTrue(testItem.getDefense() == 4);
	}
	
	@Test
	public void testGetHealth() {
		assertTrue(testItem.getHealth() == 5);
	}
	
	@Test
	public void testGetName() {
		assertTrue(testItem.getName().equals("testItem"));
	}
	
	@Test
	public void testGetType() {
		assertTrue(testItem.getType().equals("item"));
	}
	
	@Test
	public void testSetName() {
		testItem.setName("testName");
		assertTrue(testItem.getName().equals("testName"));
	}
	
	@Test
	public void testScaleWithLevel() {
		testItem.scaleWithLevel(23);
		System.out.println(testItem.getAttack());
		assertTrue(testItem.getAttack() == 9);
		assertTrue(testItem.getDefense() == 12);
		assertTrue(testItem.getHealth() == 15);
	}
}
