package com.patmendus.endlesslabyrinth;

import static org.junit.Assert.*;

import org.junit.Test;

import com.patmendus.endlesslabyrinth.adventurer;
import com.patmendus.endlesslabyrinth.item;

public class adventurerTest {

	adventurer testAdventurer = new adventurer();
	item sampleItem = new item("testItem", 5, 5, 5);
	item emptyItem = new item("Empty",0,0,0);
	
	//CONSTRUCTOR TEST
	@Test
	public void testAdventurer() {
		assertTrue(testAdventurer.getItem1().getName().equals("Empty"));
		assertTrue(testAdventurer.getItem2().getName().equals("Empty"));
		assertTrue(testAdventurer.getItem3().getName().equals("Empty"));
		assertTrue(testAdventurer.getAttack() == 3);
		assertTrue(testAdventurer.getDefense() == 3);
		assertTrue(testAdventurer.getHealth() == 3);
	}

	//ACCESSOR TESTS
	@Test
	public void testGetAttack() {
		assertTrue(testAdventurer.getAttack() == 3);
	}

	@Test
	public void testGetDefense() {
		assertTrue(testAdventurer.getDefense() == 3);
	}

	@Test
	public void testGetHealth() {
		assertTrue(testAdventurer.getHealth() == 3);
	}
	
	@Test
	public void testGetItem1() {
		item testItem = testAdventurer.getItem1();
		assertTrue(testItem.equals(testAdventurer.getItem1()));
	}

	@Test
	public void testGetItem2() {
		item testItem = testAdventurer.getItem2();
		assertTrue(testItem.equals(testAdventurer.getItem2()));
	}

	@Test
	public void testGetItem3() {
		item testItem = testAdventurer.getItem3();
		assertTrue(testItem.equals(testAdventurer.getItem3()));
	}

	@Test
	public void testGetTempDefense() {
		assertTrue(testAdventurer.getTempDefense() == 3);
	}
	
	//MUTATOR TESTS
	@Test
	public void testSetItem1() {
		testAdventurer.setItem1(sampleItem);
		assertTrue(sampleItem.equals(testAdventurer.getItem1()));
	}

	@Test
	public void testSetItem2() {
		testAdventurer.setItem2(sampleItem);
		assertTrue(sampleItem.equals(testAdventurer.getItem2()));
	}

	@Test
	public void testSetItem3() {
		testAdventurer.setItem3(sampleItem);
		assertTrue(sampleItem.equals(testAdventurer.getItem3()));
	}

	//METHOD TESTS
	@Test
	public void testCalcAttributes() {
		item testItem = new item("testItem",2,3,4);
		testAdventurer.levelUp();
		testAdventurer.levelUp();
		testAdventurer.levelUp();
		testAdventurer.setItem1(testItem);
		testAdventurer.setItem2(testItem);
		testAdventurer.setItem3(testItem);
		testAdventurer.calcAttributes();
		assertTrue(testAdventurer.getAttack() == 12);
		assertTrue(testAdventurer.getDefense() == 15);
		assertTrue(testAdventurer.getHealth() == 18);
	}

	@Test
	public void testLevelUp() {
		testAdventurer.levelUp();
		assertTrue(testAdventurer.getAttack() == 4);
		assertTrue(testAdventurer.getDefense() == 4);
		assertTrue(testAdventurer.getHealth() == 4);
	}
}
