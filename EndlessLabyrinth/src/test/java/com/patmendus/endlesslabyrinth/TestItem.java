package com.patmendus.endlesslabyrinth;

import static org.junit.Assert.*;

import org.junit.Test;

public class itemTest {

	item testItem = new item("testItem",3,4,5);
	
	@Test
	public void testItem() {
		assertTrue(testItem.getName().equals("testItem"));
		assertTrue(testItem.getAttack() == 3);
		assertTrue(testItem.getDefense() == 4);
		assertTrue(testItem.getHealth() == 5);
		assertTrue(testItem.getType().equals("item"));
	}
	
}
